'use strict';
const Exception = require('@mallocapps/rest-exception');

module.exports = function (req, res, next) {
    let APIKey = req.headers['x-api-key'] || "";
    if (APIKey !== process.env.API_KEY) {
        throw new Exception("Invalid Key");
    }

    next();
};
